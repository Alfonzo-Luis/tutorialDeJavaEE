package parte26DesafioGrupalNro2;

public interface ParaAcomodadores {

    public Salas getSala();

    /*
    10) La interfaz ParaAcomodadores deberá permitir
    la asignación y modificación de una sala a un acomodador.
     */

    public void setSala(Salas sala);

}
