package parte26DesafioGrupalNro2;

import java.util.Set;
import java.util.TreeSet;

public class Salas {

    /*
    1) Una sala de cine debe contener la capacidad de la sala,
    la pel�cula que se proyecta en la misma, el nombre de la sala
    y el listado de espectadores asignados a la misma.
     */
    private int capacidad;
    private String pelicula;
    private String nombre;
    private Set<Espectadores> espectadores;

    public Salas(int capacidad, String nombre) {
        this.capacidad = capacidad;
        this.nombre = nombre;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    /*
    2) Las Salas no deben permitir la asignaci�n de una lista de Espectadores superior a su capacidad.
     */
    public void setEspectadores(Set<Espectadores> espectadores) {

        if(espectadores.size()>this.capacidad) {
            System.out.println("CAPACIDAD DE ESPECTADORES SUPERADA.");
        }else {
            this.espectadores = espectadores;
        }

    }

    public void getEspectadores() {
        /*
        3) Se debe poder listar los espectadores de una sala,
        pero en caso de que no haya sido asignada se debe capturar
        el error emitiendo el mensaje "SIN ESPECTADORES CARGADOS".
         */

        for (Espectadores espectador : espectadores) {
            System.out.println(espectador.toString());
        }

    }

    @Override
    public String toString() {
        return "Salas [capacidad=" + capacidad + ", pelicula=" + pelicula + ", nombre=" + nombre;
    }

}
