package parte26DesafioGrupalNro2;

public abstract class Personas {

    /*
    4) El Programa debe por lo menos una clase abstracta Personas
    de la cual hereden Espectadores y Empleados

    5) La clase Personas deber� tener por lo menos dos m�todo abstractos:

     a) Uno que permitir� ver si la persona es un Espectador,
     un Acomodador o un Empleado dependiendo de qu� clase sea.

     b) Uno que permitir� visualizar todos los datos de la persona.

     */

    private String nombre;
    private int edad;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public abstract String getTipo();

    public abstract String toString();
}
