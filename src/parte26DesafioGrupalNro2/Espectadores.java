package parte26DesafioGrupalNro2;

public class Espectadores extends Personas{

    /*
    6) Los Espectadores adem� de sus datos personales deben estar asignados
    a una butaca definida por la Fila (una letra) y la Silla (un entero entero).
     */
    private String fila;
    private int silla;

    public Espectadores(String nombre, int edad, String fila, int silla) {
        // TODO Auto-generated method stub
        super.setNombre(nombre);
        super.setEdad(edad);
        this.fila = fila;
        this.silla = silla;
    }

    @Override
    public String getTipo() {
        // TODO Auto-generated method stub
        return "Espectador";
    }

    public String getButaca() {
        return this.fila+"-"+this.silla;
    }


    @Override
    public String toString() {
        return super.getNombre()+"{" +
                "fila='" + fila + '\'' +
                ", silla=" + silla +
                '}';
    }
}
