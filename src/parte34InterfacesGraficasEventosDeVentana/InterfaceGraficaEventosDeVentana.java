package parte34InterfacesGraficasEventosDeVentana;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;


class InterfaceGraficaEventosDeVentana {

    public static void main(String[] args) {

        // TODO Auto-generated method stub
        MarcoEventoVentana miMarco = new MarcoEventoVentana();

        /*
         * en este caso utilizamos DISPOSE_ON_CLOSE para poder dar
         * tiempo a que aparezcan los eventos de proceso de cierre
         * y de cierre
         */
        miMarco.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        /** Hay que agregar el escuchador**/

        miMarco.addWindowListener(new EventosDeVentana());

    }
}

class MarcoEventoVentana extends JFrame {

    public MarcoEventoVentana() {

        setTitle("Eventos de Ventana");

        setBounds(30, 30, 600, 500);

        setVisible(true);

    }

}

class EventosDeVentana implements WindowListener{

    @Override
    public void windowOpened(WindowEvent e) {
        System.out.println("EN PROCESO DE CIERRE");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("CERRANDO");
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.out.println("CERRADA");
    }

    @Override
    public void windowIconified(WindowEvent e) {
        System.out.println("MINIMIZAR");
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        System.out.println("MAXIMIZAR");
    }

    @Override
    public void windowActivated(WindowEvent e) {
        System.out.println("VENTANA ACTIVADA");
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        System.out.println("DESACTIVADA");
    }
}
