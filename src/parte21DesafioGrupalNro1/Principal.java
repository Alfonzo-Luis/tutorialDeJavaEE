package parte21DesafioGrupalNro1;

class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Facultad frr = new Facultad("UTN-FRR");
		
		/*
		 * 1) Agregar Carreras a una Facultad. 
		 */
		
		frr.agregarCarrera("TSP");
		frr.agregarCarrera("ISI");
		frr.agregarCarrera("IQ");
		
		/*
		 * 2) Eliminar Carreras de la facultad
		 */
		
		frr.eliminarCarrera("IQ");
		
		/*
		 * 3) Eliminar Estudiantes de una facultad implica que se elimine el estudiante de cada una de las materias 
		 * a las cuales se inscribió. 
		 */
		
		
		/*
		 * Agregar Profesor a una Materia.
		 */
		
		Profesor titular = new Profesor(15000, 2, "Facundo", "Uferer", 3553);
		
		/*
		 * 4) Agregar materias a una Carrera
		 */
		
		frr.agregarMateriaCarrera("TSP", "Laboratorio de Computación II", titular);
		
		frr.agregarMateriaCarrera("ISI", "Laboratorio 2", titular);
		
		frr.agregarMateriaCarrera("ISI", "Laboratorio 1", titular);
		
		/*
		 * 5) Eliminar materias a una Carrera
		 */
		
		frr.eliminarMateriaDeCarrera("ISI", "Laboratorio 2");
		
		/*
		 * 6) Encontrar materias de una carrera en particular indicando el nombre de la materia. 
		 * Si la materia existe nos deberá preguntar si deseamos eliminar.
		 */
		
		frr.encontrarMateriaEnCarrera("TSP", "Laboratorio de Computación II");
		
		/*
		 * 7) Agregar Estudiantes a una Materia.
		 */
		
		Estudiante estudiante = new Estudiante("Carlos","Susuky" , 2548);
		Estudiante estudiante2 = new Estudiante("carla","Motorolla" , 3525);
		
		frr.agregarEstudiante("TSP", "Laboratorio de Computación II", estudiante);
		frr.agregarEstudiante("TSP", "Laboratorio de Computación II", estudiante2);
		
		/*
		 * 8) Eliminar Estudiante de una Materia
		 */
		
		frr.eliminarEstudiante("TSP", "Laboratorio de Computación II", 2548);
		
		/*
		 * 9) Modificar el Profesor de la materia
		 */
		
		Profesor nuevoProvesor = new Profesor(15478, 2, "Juan", "Carlos", 1212);
		
		frr.modificarTitular("TSP", "Laboratorio de Computación II", nuevoProvesor);
		
		System.out.println(frr.toString());

	}

}
