package parte21DesafioGrupalNro1;

import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;

class Carrera implements Informacion {

	private String nombre;
	
	private Set<Materia> materias = new HashSet<Materia>();

	public Carrera(String nomCar) {
		this.nombre = nomCar;
	}

	public void agregarMateria(String nombre, Profesor titular) {
		this.materias.add(new Materia(nombre, titular));
	}

	public void eliminarMateria(String nombre) {
		
		Iterator<Materia> eliminar = materias.iterator();
		
		while (eliminar.hasNext()) {
			if (eliminar.next().getNombre().equals(nombre)) {
				eliminar.remove();
				System.out.println(nombre + " fue eliminada");
			}
		}
	}

	public Materia encontrarMateria(String nombreMateria) {
		for (Materia materia : materias) {
			if(materia.getNombre().equals(nombreMateria)) {
				return materia;
			}
		}
		return null;
	}

	public void eliminarEstudiante(int nroLegajo) {
		for (Materia materia : materias) {
			materia.eliminarEstudiante(nroLegajo);
		}
	}

	/*
	 * IMPLEMENTACIÓN DE LA INTERFACE
	 */

	@Override
	public int verCantidad() {
		// TODO Auto-generated method stub
		return this.materias.size();
	}

	/*
	 * 12) El método listarContenidos() de la interface Información lista los elementos de la clase contenida, 
	 * es decir que de la clase Facultad se listará las Carreras, de la clase Carreras las materias, etc. 
	 * Siempre en orden alfabético.
	 */
	
	@Override
	public String listarContenidos() {
		return materias.toString();
	}

	/*
	 * GETERS AND SETERS
	 */

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<Materia> getMaterias() {
		return materias;
	}

	public void setMaterias(Set<Materia> materias) {
		this.materias = materias;
	}

	@Override
	public String toString() {
		return "\n \n \t Carrera [nomCar=" + nombre + ", materias=" + materias + "]";
	}

}
