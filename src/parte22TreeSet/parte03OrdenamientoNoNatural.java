package parte22TreeSet;

import java.util.Comparator;
import java.util.TreeSet;

class parte03OrdenamientoNoNatural {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Pelicula comparador = new Pelicula();
		
		TreeSet<Pelicula> peliculas = new TreeSet<>(comparador);
		
		peliculas.add(new Pelicula("Duro de Matar"));
		peliculas.add(new Pelicula("El se�or de los Anillos"));
		peliculas.add(new Pelicula("E.T."));
		peliculas.add(new Pelicula("Piratas del Caribe en el Fin del Mundo"));
		peliculas.add(new Pelicula("Blade Runer"));
		
		for (Pelicula pelicula : peliculas) {
			System.out.println(pelicula.getNombre());
		}
		
	}

}

/*
 * La implementaci�n de la interface Comparator nos permitir� ordenamientos
 * personalizados, es decir NO NATURALES
 */
class Pelicula implements Comparator<Pelicula> {

	private String nombre;

	Pelicula(){
		
	}
	Pelicula(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int compare(Pelicula p1, Pelicula p2) {
		// TODO Auto-generated method stub
		/*
		 * por ejemplo un ordanamiento por seg�n la cantidad de caracteres del nombre de
		 * las pel�culas.
		 */
		int cantCaracteres1 = p1.getNombre().length();
		int cantcaracteres2 = p2.getNombre().length();

		if (cantCaracteres1 < cantcaracteres2)
			return -1;
		else if (cantCaracteres1 > cantcaracteres2)
			return 1;
		else
			return 0;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
