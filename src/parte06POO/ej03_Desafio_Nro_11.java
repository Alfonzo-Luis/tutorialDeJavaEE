package parte06POO;

import java.util.Random;

public class ej03_Desafio_Nro_11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CuentaCorriente cta1 = new CuentaCorriente("Carlitos", 2000);
		CuentaCorriente cta2 = new CuentaCorriente("Anita", 3000);
		
		CuentaCorriente.Transferencia(cta1, cta2, 500);
		
		System.out.println(cta1.toString());
		System.out.println(cta2.toString());

	}

}

class CuentaCorriente {

	/*
	 * Las variables con el modificador de accedo private logran la ENCAPSULACI�N.
	 * Seg�n Wikipedia: Ocultamiento del estado de los datos de un objeto de manera
	 * que solo se pueda cambiar mediante las operaciones definidas para ese objeto.
	 * Esta acci�n protege a los datos de su modificaci�n por quien no tenga derecho
	 * a acceder a ellos. De esta forma el usuario de la clase puede obviar la
	 * implementaci�n de los m�todos y propiedades para concentrarse solo en c�mo
	 * usarlos. Por otro lado se evita que el usuario pueda cambiar su estado de
	 * maneras imprevistas e incontroladas.
	 */

	private double saldo;
	private String nombreTitular;
	private long numeroCuenta;

	/*
	 * SOBRECARGA DE CONSTRUCTORES ya que tenemos 2 constructores 
	 * pero con par�metros diferentes.
	 */
	
	public CuentaCorriente(String nomString, double saldo) {
		this.nombreTitular = nomString;
		this.saldo = saldo;

		Random aleatorio = new Random();
		this.numeroCuenta = Math.abs(aleatorio.nextLong());
	}
	
	public CuentaCorriente(String nomString) {
		this.nombreTitular = nomString;
		this.saldo = 0;

		/*
		 * Si la asignaci�n de n�mero de cuenta se usa
		 * tantas veces y de la misma forma
		 * es necesario crear un m�todo que haga esto y al que 
		 * simplemente lo llamemos
		 */
		
		Random aleatorio = new Random();
		this.numeroCuenta = Math.abs(aleatorio.nextLong());
	}



	public String ingresarDiner(double dinero) {
		/*
		 * Si un m�todo debe RETORNAR algo y el RETURN est�
		 * dentro de un IF es necesario que de una forma u otra
		 * se pueda ejecutar el RETURN, en este caso
		 * se ocup� el ELSE para que SI o SI se ejecute
		 * el RETURN  
		 */
		
		if(dinero >0) {
			this.saldo += dinero;
			return "Se ha ingresado $"+ dinero;
		}else {
			return "no se puede ingresar un valor negativo";
		}
	}

	public void sacarDinero(double dinero) {
		this.saldo -= dinero;
	}

	/*
	 * Usamos un m�todo STATIC porque como probablemnte las transferencias la
	 * realizan los bancos es necesario que el m�todo sea de la clase y no de los
	 * objetos. para que un objeto NO LA PUEDA UTILIZAR y tambi�n que el m�todo
	 * pueda usarse sin la necesidad de isntanciarse.
	 */

	
	public static void Transferencia(CuentaCorriente ctaSalida, CuentaCorriente ctaEntra, double dinero) {
		ctaEntra.saldo += dinero;
		ctaSalida.saldo -= dinero;
	}

	@Override
	public String toString() {
		return "\n nombreTitular = " + nombreTitular+
				"\n saldo = " + saldo + 
				"\n numeroCuenta = " + numeroCuenta + "\n";
	}
	
	

}
