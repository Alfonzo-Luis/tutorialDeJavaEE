package parte06POO;

public class ej04UsoFacultad {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Estudiante estudiante1 = new Estudiante("Vanina");
		
		Estudiante estudiante2 = new Estudiante("Jose");
		
		System.out.println(estudiante1.getDatosEstudiante());
		
		estudiante1.setMateria("Lab Prog II");
		System.out.println(estudiante1.getDatosEstudiante());

	}

}

class Estudiante{
	
	/*
	 * la variable nombre tienen la palabra reservada final
	 * para que una vez que se de un valor a la variable 
	 * este sea constante y no se pueda modificar el nombre. 
	 * Si intentaramos crear un par�metro que modifique nombre
	 * como es una constante ahora, ya no podr� ser modificado. 
	 */
	private final String nombre;
	private String materia;
	
	public Estudiante(String nombre) {
		this.nombre = nombre;
		this.materia = "Lab Prog. I";
	}
	
	public void setMateria(String materia) {
		this.materia = materia;
	}
	
	public String getDatosEstudiante() {
		return this.nombre+" esta inscripto en la materia "+this.materia;
	}
		
	
}
