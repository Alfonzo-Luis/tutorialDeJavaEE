package parte06POO;

import java.util.Scanner;

public class ej02Celulares {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner teclado = new Scanner(System.in); // variable para capturar el ingreso de teclado

		System.out.println("Tama�o");
		int tamanio = teclado.nextInt();

		System.out.println("Tipo:");
		String tipo = teclado.next();

		System.out.println("Stock:");
		int stock = teclado.nextInt();

		System.out.println("Precio:");
		double precio = teclado.nextDouble();

		// contructor con par�metros
		Movil celular = new Movil(precio, tamanio, tipo, stock);

		// Constructor sin par�metros
		Movil celular2 = new Movil();

		System.out.println(celular.getInfo());
		System.out.println(celular2.getInfo());

		System.out.print("\n\nIngrese cantidad de celulares a vender");

		System.out.print(celular.vender(teclado.nextInt()));

	}

}

class Movil extends Producto {

	// declaraci�n de variables privadas accesibles solo por la clase:

	private int tamanioPantalla;
	private String tipo;
	private int stock;

	/*
	 * Esta clase tiene DOS CONSTRUCTORES por lo que tienen sobrecarga de m�todos, o
	 * en este caso sobrecarga de constructores
	 */

	public Movil(double precio, int tamanioPantalla, String tipo, int stock) {
		super(precio);
		this.tamanioPantalla = tamanioPantalla;
		this.tipo = tipo;
		this.stock = stock;
	}

	public Movil() {
		super(20);
		this.tamanioPantalla = 12;
		this.tipo = "generico";
		this.stock = 0;
	}

	public String getInfo() {

		// retorna las propiedades del objeto

		return "\nTama�o pantalla:" + this.tamanioPantalla + "\nTipo:" + this.tipo + "\nTipo:" + this.stock
				+ "\nPrecio:" + super.getPrecio();
	}

	public String vender(int cantidad) {
		// disminuye la variable cantidad
		this.stock = this.stock - cantidad;
		return "Vendidos: " + cantidad + " " + this.tipo + "\nEn Stock: " + this.stock;
	}

}

class Producto {

	private double precio;

	Producto(double precio) {
		this.precio = precio;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

}
