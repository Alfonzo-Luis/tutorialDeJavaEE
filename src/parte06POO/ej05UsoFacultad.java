package parte06POO;

public class ej05UsoFacultad {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//System.out.println(Estudiante2.getLegajoAlmacenado()); //es un m�todo EST�TICO por eso puede ser accedido sin instanciar la clase

		Estudiante2 estudiante1 = new Estudiante2("VANINA");

		System.out.println(estudiante1.getDatosEstudiante());
		
		Estudiante2 estudiante2 = new Estudiante2("JOSE");
		estudiante2.setMateria("Programacion II");
		System.out.println(estudiante2.getDatosEstudiante());
		
		Estudiante2 estudiante3 = new Estudiante2("ANHNA");
		System.out.println(estudiante3.getDatosEstudiante());
		
		//System.out.println(Estudiante2.getLegajoAlmacenado());

	}

}

class Estudiante2{
	

	private final String nombre;
	private String materia;
	private int nroLegajo;
	
	/*
	 * el modificador static nos indica que la variable
	 * pertenece a la clase, por lo tanto no se copia a la 
	 * instancia de los objetos creados a trav�z de esta clase. 
	 * por lo tanto el valor almacenado en ella ser� persiste
	 * para acceder a ella no hace falta instanciar la clase. 
	 */
	
	private static int legajoAlmacenado = 1;
	
	public Estudiante2(String nombre) {
		this.nombre = nombre;
		this.materia = "Programacion I";
		this.nroLegajo = legajoAlmacenado;
		legajoAlmacenado++;
		
	}
	
	public void setMateria(String materia) {
		this.materia = materia;
	}
	
	public String getDatosEstudiante() {
		return this.nombre+" esta inscripto en la materia "+this.materia +" y su legajo es "+this.nroLegajo;
	}
	
	public static String getLegajoAlmacenado() {
		return "nro Legajo: "+ Estudiante2.legajoAlmacenado;
	}
		
	
}
