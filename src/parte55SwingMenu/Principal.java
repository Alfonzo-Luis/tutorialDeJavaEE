package parte55SwingMenu;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 400, 400);
        add(new PanelPrincipal());
        setTitle("Radio BUtton");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    public PanelPrincipal(){

        /**Para crear menu primero hay que crear
         * una barra donde colocar**/

        JMenuBar barraDeMenu = new JMenuBar();

        //---- elemntos del men� ----

        JMenu archivo = new JMenu("archivo");
        JMenu edicion = new JMenu("edicion");
        JMenu herramientas = new JMenu("herramientas");

        /**Luego de crear los items del menu, es necesario
         * agregarlo a la barra de menu**/

        barraDeMenu.add(archivo);
        barraDeMenu.add(edicion);
        barraDeMenu.add(herramientas);

        /**Luego de crear y agregar los menues
         * agregamos los �tems para cada menu**/

        //----- elementos de Archivo ----

        JMenuItem guardar = new JMenuItem("Guardar");
        JMenuItem guardarComo = new JMenuItem("Guradar como");

        archivo.add(guardar);
        archivo.add(guardarComo);

        //---items de edicion---
        JMenuItem cortar = new JMenuItem("Cortar");
        JMenuItem copiar = new JMenuItem("Copiar");
        JMenuItem pegar = new JMenuItem("Pegar");

        edicion.add(cortar);
        edicion.add(copiar);
        edicion.add(pegar);

        //---items de herramientas---
        JMenuItem opciones = new JMenuItem("Opciones");

        /**A preferencias le colocamos esto para que
         * despliegue sub-menues**/
        JMenuItem preferencias = new JMenu("Preferencias");

        herramientas.add(opciones);
        herramientas.addSeparator(); /**Separador para hacer m�s prolijo **/
        herramientas.add(preferencias);

        //---items de herramientas -> preferencias---
        JMenuItem generales = new JMenuItem("Generales");
        JMenuItem ayuda = new JMenuItem("Ayuda");

        ayuda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Apretaste en Ayuda");
            }
        });

        preferencias.add(generales);
        preferencias.add(ayuda);

        add(barraDeMenu);

    }

}
