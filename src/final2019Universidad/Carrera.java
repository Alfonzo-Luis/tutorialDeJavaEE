package final2019Universidad;

import java.util.Set;

class Carrera {
	
	private Set<Materia> materias;

	public Carrera(Set<Materia> materias) {
		super();
		this.materias = materias;
	}
	
	

	public Set<Materia> getMaterias() {
		return materias;
	}



	public void setMaterias(Set<Materia> materias) {
		this.materias = materias;
	}



	@Override
	public String toString() {
		return "Carrera [materias=" + materias + "]";
	}

	

}
