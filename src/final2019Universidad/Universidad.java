package final2019Universidad;

import java.util.Set;

class Universidad {
	
	private String direccion;
	private String nombre; 
	private int provincia;
	private Set<Facultad> facultades;
	
	public Universidad(String direccion, String nombre, int provincia) {
		super();
		this.direccion = direccion;
		this.nombre = nombre;
		this.provincia = provincia;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getProvincia() {
		return provincia;
	}

	public void setProvincia(int provincia) {
		this.provincia = provincia;
	}

	public Set<Facultad> getFacultades() {
		return facultades;
	}

	public void setFacultades(Set<Facultad> facultades) {
		this.facultades = facultades;
	}

	@Override
	public String toString() {
		return "Universidad [direccion=" + direccion + ", nombre=" + nombre + ", provincia=" + provincia
				+ ", facultades=" + facultades + "]";
	}
	
	

}
