package parte31InterfacesGraficas;

import javax.swing.*;
import java.awt.*;

public class parte03MetodosJFrame {

    public static void main(String[] args) {

        MyJFrmae2 ventana = new MyJFrmae2();

    }

}

class MyJFrmae2 extends JFrame {

    public MyJFrmae2() {

        setSize(500, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //coloca la ventana en el centro
        setLocationRelativeTo(null);

        //COLOCAR UN �CONO PERSONALIZADO A MI VENTANA
        Toolkit sistema = Toolkit.getDefaultToolkit();
        Image icono = sistema.getImage("C:\\Users\\juanf\\workspace\\tutorialDeJavaEE\\src\\parte31InterfacesGraficas\\gatito.jpg");
        setIconImage(icono);

        setTitle("Mi primer Ventana"); //coloca el t�tulo a la venta

        //con MAXIMIZED_BOTH la ventana se maximiza
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        setVisible(true);

        PrimerPanel lamina= new PrimerPanel();
        add(lamina);
    }

}

class PrimerPanel extends JPanel {

    // un panel tiene como tamanio maximo el contenedor donde se encuentra

    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        // PINTAR UN PANEL DE UN COLOR DETERMINADO
        // creamos el color
        Color c = new Color(255, 128, 155);
        // le damos el color
        g.setColor(c);

        //PERSONALIZAMOS LA LETRA
        //cre el objeto fuente
        Font letra = new Font("Verdana", Font.BOLD, 30);
        //setea la fuente al elemento
        g.setFont(letra);

        // sobre escritura del metodo JPanel
        g.drawString("Primer panel", 25, 25);
    }

}

