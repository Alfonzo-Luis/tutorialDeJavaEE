package parte31InterfacesGraficas;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class parte05EventosDeRaton {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        MarcoVentana ventana = new MarcoVentana();

        ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //pone en escucha al marco
        ventana.addMouseMotionListener(new EventosDeRaton());
    }
}

class MarcoVentana extends JFrame{

    public MarcoVentana() {
        setBounds(30, 30, 600, 500);
        setVisible(true);
    }
}

class EventosDeRaton implements MouseMotionListener {

    @Override
    public void mouseDragged(MouseEvent arg0) {
        // TODO Auto-generated method stub
        System.out.println("ARRASTRANDO ->");
    }

    @Override
    public void mouseMoved(MouseEvent arg0) {
        // TODO Auto-generated method stub
        System.out.println("MOVIENDO ->");
    }
}
