package parte29Hilos;

public class parte01CrearHilos {

    public static void main(String[]args){


        Runnable r = new Contador(1);
        Thread hilo = new Thread(r);

        Runnable r2 = new Contador(2);
        Thread hilo2 = new Thread(r2);

        //INICIO DEL HILO
        hilo.start();

        hilo2.start();
    }

}

class Contador implements Runnable {

    private int nroHilo;

    public Contador(int nroHilo){
        this.nroHilo = nroHilo;
    }

    @Override
    public void run() {
        for(int i=0; i<=100; i++) {
            try {

                Thread.sleep(100); //durme el hilo unos milisenundos
                System.out.println(this.nroHilo+" -> "+i+" ");

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
