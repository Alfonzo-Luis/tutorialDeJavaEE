package parte47SwingRadioButon;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 400, 400);
        add(new PanelBotones());
        setTitle("Swing");
        setVisible(true);
    }

}

class PanelBotones extends JPanel{

    private JRadioButton boton1, boton2, boton3;

    public PanelBotones(){

        ManejoBotones escuchador = new ManejoBotones();
        /** Agrupa los botones para que funcionen en conjunto
         * y solo exista un bot�n activado a la vez. **/
        ButtonGroup grupoBotones = new ButtonGroup();

        boton1 = new JRadioButton("Masculino");
        boton2 = new JRadioButton("Femenino");
        boton3 = new JRadioButton("Neutro");

        /** Agregar los escuchadores a lso botones**/
        boton1.addActionListener(escuchador);
        boton2.addActionListener(escuchador);
        boton3.addActionListener(escuchador);

        /**para incorporar los botones al grupo**/
        grupoBotones.add(boton1);
        grupoBotones.add(boton2);
        grupoBotones.add(boton3);

        /**Agregar los botones a la interface**/
        add(boton1);
        add(boton2);
        add(boton3);
    }

    private class ManejoBotones implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource()==boton1) System.out.println(boton1.getText());
            if(e.getSource()==boton2) System.out.println(boton2.getText());
            if(e.getSource()==boton3) System.out.println(boton3.getText());
        }
    }

}
