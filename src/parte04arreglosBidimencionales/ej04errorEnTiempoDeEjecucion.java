package parte04arreglosBidimencionales;

public class ej04errorEnTiempoDeEjecucion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * EL primer �ndice es la LINEA
		 * EL segundo �ndice es la COLUMNA
		 * Es importante que entendamos esta estructura
		 * de almacenamiento para poder saber como recorrerlo
		 */
		
		int [][] matriz = new int [4][3];
		
		matriz[0][0] = 1;
		matriz[0][1] = 2;
		matriz[0][2] = 3;
		matriz[0][3] = 3; //aqu� saltar� el error en tiempo de ejecuci�n
		
		/*
		 * el error en tiempo de ejecuci�n que se ve aqu�
		 * es porque se intent� cargar algo fuera del array
		 * en este caso particular el index esta fuera, es decir
		 * que se exedi� en los l�mites del array. 
		 */
		
		matriz[1][0] = 4;
		matriz[1][1] = 5;
		matriz[1][2] = 6;
		
		matriz[2][0] = 7;
		matriz[2][1] = 8;
		matriz[2][2] = 9;
		
		matriz[3][0] = 10;
		matriz[3][1] = 11;
		matriz[3][2] = 12;
		
		System.out.println("\nMOSTRAR COMO MATRIZ");
		
		for(int x=0; x<4; x++) {
			for(int y=0; y<3;y++) {
				System.out.print(matriz[x][y]+" ");
			}
			System.out.println("");
		}


	}

}
