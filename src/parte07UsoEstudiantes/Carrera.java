package parte07UsoEstudiantes;

class Carrera {
	
	private String nombre;
	private Estudiante arrayEstudiante[];
	private int posicionArray=0;
	
	public Carrera(String nombre, int cantEstudiantes) {
		this.nombre = nombre;
		this.arrayEstudiante = new Estudiante[cantEstudiantes];
	}
	
	public String getNombreCarrera() {
		return this.nombre;
	}
	
	private Estudiante getEstudiante(int nroLegajo) {
		return arrayEstudiante[nroLegajo];
	}
	
	public void nuevoEstudiante(String nombre, double promedio) {
		Estudiante nuevoEstudiante = new Estudiante(this, nombre, promedio);
		arrayEstudiante[posicionArray]=nuevoEstudiante;
		posicionArray++;
	}
	
	public void borrarEstudiante(String nombreEstudiante) {
		for (int i = 0; i < arrayEstudiante.length; i++) {
			if(this.getEstudiante(i)!=null) {
				if(arrayEstudiante[i].getNombreEstudiante().equals(nombreEstudiante)) {
					arrayEstudiante[i]=null;
				}
			}
		}
	}
	
	public void setPromedio(String nombreEstudiante, double promedioEstudiante) {
		for (int i = 0; i < arrayEstudiante.length; i++) {
			if(getEstudiante(i)!=null) {
				if(arrayEstudiante[i].getNombreEstudiante().equals(nombreEstudiante)) {
					arrayEstudiante[i].setPromedio(promedioEstudiante);
				}
			}
		}
	}
	
	public void getEstudiantes() {
		for (int i = 0; i < arrayEstudiante.length; i++) {
			if(this.getEstudiante(i)!=null) {
				System.out.println(this.getEstudiante(i));
			}
		}
	}
	
	public void getDatosEstudiante(String nombreEstudiante) {
		for (int i = 0; i < arrayEstudiante.length; i++) {
			if(this.getEstudiante(i)!=null) {
				if(arrayEstudiante[i].getNombreEstudiante().equals(nombreEstudiante)) {
					System.out.println(this.getEstudiante(i));
				}
			}
		}
	}
	

}
