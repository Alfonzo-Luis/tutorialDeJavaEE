package parte44LayoutsGridLayouts;

import javax.swing.*;
import java.awt.*;

public class GridLayoutsPrincipal {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        FramePrincipal framecito = new FramePrincipal();
        framecito.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        framecito.setVisible(true);
    }
}

class FramePrincipal extends JFrame {

    public FramePrincipal() {
        setTitle("Border Layouts");
        setBounds(600, 350, 600, 300);

        PanelPrincial panelcito = new PanelPrincial();
        add(panelcito);

    }

}

class PanelPrincial extends JPanel {

    public PanelPrincial() {

        /**Tipos de Disposiciones de Layouts**/
        setLayout(new GridLayout(2,2));
        add(new JButton("UNO"));
        add(new JButton("DOS"));
        add(new JButton("TRES"));
        add(new JButton("CUATRO"));

    }
}