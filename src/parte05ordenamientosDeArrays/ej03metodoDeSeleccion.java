package parte05ordenamientosDeArrays;

import java.util.Arrays;

public class ej03metodoDeSeleccion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int arreglo[] = { 6, 4, 2, 5, 3, 1 };
		int cont = 0;
		
		for (int i = 0; i < arreglo.length-1; i++) {
			int minimo = i;
			for (int j = i+1; j < arreglo.length; j++) {
				if(arreglo[j]<arreglo[minimo]) {
					minimo = j;
					cont++;
				}
			}
			
			int aux = arreglo[i];
			arreglo[i] = arreglo[minimo];
			arreglo[minimo] = aux;
			
			// esta estructura muestra la matriz para que podamos ver como se va ordenando
			System.out.print(Arrays.toString(arreglo));
			System.out.println(" ");
			//fin for para mostrar
			
		}
		
		System.out.println("PASOS: "+cont);

	}

}
