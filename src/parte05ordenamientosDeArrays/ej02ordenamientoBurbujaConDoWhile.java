package parte05ordenamientosDeArrays;

public class ej02ordenamientoBurbujaConDoWhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int arreglo[] = { 6, 4, 2, 5, 3, 1 };
		int cont = 0;
		boolean desorden;

		do {
			desorden = false;
			for (int i = 0; i < arreglo.length - 1; i++) {
				if (arreglo[i] > arreglo[i + 1]) {
					int tmp = arreglo[i + 1];
					arreglo[i + 1] = arreglo[i];
					arreglo[i] = tmp;
					desorden = true;
					cont++;
					// esta estructura muestra la matriz para que podamos ver como se va ordenando
					for (int elemento : arreglo) {
						System.out.print(elemento + " ");
					}
					System.out.println(" ");
					// fin for para mostrar
				}
			}

		} while (desorden);
		System.out.println("PASOS: " + cont);
	}
}
