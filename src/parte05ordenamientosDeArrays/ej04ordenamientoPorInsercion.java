package parte05ordenamientosDeArrays;

import java.util.Arrays;

public class ej04ordenamientoPorInsercion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int arreglo[] = { 6, 4, 2, 5, 3, 1 };
		int cont = 0;

		int posicion, aux;

		for (int i = 0; i < arreglo.length; i++) {
			posicion = i;
			aux = arreglo[i];

			while ((posicion > 0) && (arreglo[posicion - 1] > aux)) {
				arreglo[posicion] = arreglo[posicion - 1];
				posicion--;
			}
			arreglo[posicion] = aux;
			cont++;
			// esta estructura muestra la matriz para que podamos ver como se va ordenando
			System.out.print(Arrays.toString(arreglo));
			
			System.out.println(" ");
			// fin for para mostrar
		}
		
		System.out.println("PASOS: "+cont);

	}

}
