package parte46FrameVacio;

import javax.swing.*;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 400, 400);
        add(new PanelPrincipal());
        setTitle("Radio BUtton");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    public PanelPrincipal(){

    }

}
