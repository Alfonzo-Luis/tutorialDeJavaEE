package parte50SwingTextAreas;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 450, 450);
        add(new PanelPrincipal());
        setTitle("Text �reas");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    public PanelPrincipal(){
        /**Utilzamos el constructor que nos permite definir la cantidad de filas y la cantidad de columnas**/

        JTextArea areaTexto = new JTextArea(20, 30);

        /**setLIneWrap hace que cuando lleguemos al final del cuadro salte al sigueinte reglon**/
        areaTexto.setLineWrap(true);

        /**El JScrollPane permite que cuando el texto supere el espacio del area de texto se creen las barras
         * de desplazamiento**/
        JScrollPane panelConScroll = new JScrollPane(areaTexto);

        add(panelConScroll);

        JButton boton = new JButton("Mostrar en consola");

        /** Para colocar en escucha el boton podr�as crear una clase intera an�nima**/
        boton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(areaTexto.getText());
            }
        });

        add(boton);
    }



}
