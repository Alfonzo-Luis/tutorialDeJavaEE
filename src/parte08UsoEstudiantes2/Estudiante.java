package parte08UsoEstudiantes2;

class Estudiante extends Persona{
	
	private Carrera carreraEstudiante;
	private double promedio;
	
	private int nroLegajo;
	private static int LegajoAlmacenado=1;
	
	//NUEVO en relaci�n al programa de la parte07...
	public Estudiante(Carrera carreraEstudiante, String nombre, double promedio, int dni, int dia, int mes, int anio) {
		super(nombre, dni, dia, mes, anio);
		this.carreraEstudiante = carreraEstudiante;
		this.promedio = promedio;
		this.nroLegajo = LegajoAlmacenado;
		Estudiante.LegajoAlmacenado++;
	}
	
	public void setPromedio(double promedio) {
		this.promedio=promedio;
	}

	/*
	 * aqu� se sobreescribi� un m�todo del padre que a su vez
	 * el padre ya lo sobre escrib�a de su padra que es Objetc.
	 * ya que todos los objetos en JAVA son hijes de OBJETC.
	 * �las clases que heredan son hijos, hijas o hijes?
	 */
	
	@Override
	public String toString() {
		return "Estudiante [carrera=" + carreraEstudiante.getNombreCarrera() + ", nombre=" + super.getNombre() + ", promedio=" + promedio
				+ ", nroLegajo=" + nroLegajo + "]";
	}
	
	private String getNombreCarrera() {
		return this.carreraEstudiante.getNombreCarrera();
	}
	
	/*
	 * �ste es un corpotamiento distinto del padre
	 */
	public String getNombreEstudiante() {
		return "El estudiante se llama: "+super.getNombre();
	}

}
