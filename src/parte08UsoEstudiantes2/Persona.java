package parte08UsoEstudiantes2;

import java.util.GregorianCalendar;

class Persona {
	
	/*
	 * Clase agregada para ver la HERENCIA de objetos
	 */
	
	private String nombre;
	private int dni;
	
	//Clase para operar con fechas -> buscar en la API
	private GregorianCalendar fechaNac; 
	
	
	public Persona(String nombre, int dni, int dia, int mes, int anio) {
		this.nombre=nombre;
		this.dni=dni;
		this.fechaNac= new GregorianCalendar(anio, mes, dia);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public GregorianCalendar getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(GregorianCalendar fechaNac) {
		this.fechaNac = fechaNac;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", dni=" + dni + ", fechaNac=" + fechaNac + "]";
	}
	
	
		

}
