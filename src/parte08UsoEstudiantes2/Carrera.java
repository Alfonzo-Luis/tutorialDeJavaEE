package parte08UsoEstudiantes2;

class Carrera {
	
	private String nombre;
	private Estudiante arrayEstudiante[];
	private int posicionArray=0;
	
	public Carrera(String nombre, int cantEstudiantes) {
		this.nombre = nombre;
		this.arrayEstudiante = new Estudiante[cantEstudiantes];
	}
	
	public String getNombreCarrera() {
		return this.nombre;
	}
	
	private Estudiante getEstudiante(int nroLegajo) {
		return arrayEstudiante[nroLegajo];
	}
	

	//CAMBIOS respecto del programa del paquete anterios
	public void nuevoEstudiante(String nombre, double promedio, int dni, int dia, int mes, int anio) {
		Estudiante nuevoEstudiante = new Estudiante(this, nombre, promedio, dni, dia, mes, anio);
		arrayEstudiante[posicionArray]=nuevoEstudiante;
		posicionArray++;
	}
	
	public void borrarEstudiante(String nombreEstudiante) {
		for (int i = 0; i < arrayEstudiante.length; i++) {
			if(this.getEstudiante(i)!=null) {
				if(arrayEstudiante[i].getNombreEstudiante().equals(nombreEstudiante)) {
					arrayEstudiante[i]=null;
				}
			}
		}
	}
	
	public void setPromedio(String nombreEstudiante, double promedioEstudiante) {
		for (int i = 0; i < arrayEstudiante.length; i++) {
			if(getEstudiante(i)!=null) {
				if(arrayEstudiante[i].getNombreEstudiante().equals(nombreEstudiante)) {
					arrayEstudiante[i].setPromedio(promedioEstudiante);
				}
			}
		}
	}
	
	public void getEstudiantes() {
		//NUEVO en relaci�n al programa de la parte07...
		for(Estudiante estudiante : arrayEstudiante) {
				System.out.println(estudiante.toString());
		}
	}
	
	public void getDatosEstudiante(String nombreEstudiante) {
		for (int i = 0; i < arrayEstudiante.length; i++) {
			if(this.getEstudiante(i)!=null) {
				if(arrayEstudiante[i].getNombreEstudiante().equals(nombreEstudiante)) {
					System.out.println(this.getEstudiante(i));
				}
			}
		}
	}
	

}
