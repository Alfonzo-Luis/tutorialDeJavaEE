package parte51SwingCasillasDeVerificacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 500, 400);
        add(new PanelPrincipal());
        setTitle("Casillas de Verificación");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    JCheckBox checkBoxNegrita, checkBoxCursiva;
    JLabel texto;

    public PanelPrincipal(){
        setLayout(new BorderLayout());

        texto = new JLabel("Hola mundo");
        texto.setFont(new Font("Courier", Font.PLAIN, 24));

        JPanel arriba = new JPanel();
        arriba.add(texto);

        checkBoxNegrita = new JCheckBox("Negrita");
        checkBoxCursiva = new JCheckBox("Cursiva");

        HacerCosasCheckBox checBoxEscuchador = new HacerCosasCheckBox();
        checkBoxNegrita.addActionListener(checBoxEscuchador);
        checkBoxCursiva.addActionListener(checBoxEscuchador);

        JPanel abajo = new JPanel();

        abajo.add(checkBoxNegrita);
        abajo.add(checkBoxCursiva);

        add(arriba, BorderLayout.NORTH);
        add(abajo, BorderLayout.SOUTH);

    }

    private class HacerCosasCheckBox implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            int tipoLetra = 0;

            if(checkBoxNegrita.isSelected()) tipoLetra += Font.BOLD;

            if(checkBoxCursiva.isSelected()) tipoLetra += Font.ITALIC;

            texto.setFont(new Font("Courier", tipoLetra, 24));

        }
    }

}
