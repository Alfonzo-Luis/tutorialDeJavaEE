package parte42LayoutsFlowLayout;

import javax.swing.*;
import java.awt.*;

public class parte01LayoutsPrincipal {
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        FramePrincipal framecito = new FramePrincipal();
        framecito.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        framecito.setVisible(true);
    }
}

class FramePrincipal extends JFrame {

    public FramePrincipal() {
        setTitle("Disposiciones");
        setBounds(600, 350, 600, 300);
        PanelConBotones panelcito = new PanelConBotones();

        /**Aqui desarrollaremos la alineaci�n**/

        FlowLayout disposicion = new FlowLayout(FlowLayout.RIGHT);
        panelcito.setLayout(disposicion);

        /** Tambi�n se pude simplificar todo en una linea de esta forma: **/
         //panelcito.setLayout(new FlowLayout(FlowLayout.RIGHT));
        /** Fin del desarrollo de la alineaci�n**/

        add(panelcito);

    }

}

class PanelConBotones extends JPanel {

    public PanelConBotones() {

        /** Podemaos agregar la disposicion del Layouts
         tanto fuera de esta clase como en su constructor:**/

        // setLayout(new FlowLayout(FlowLayout.RIGHT));

        /**o tamb�n podemos hacer as�**/
        //setLayout(new FlowLayout(FlowLayout.CENTER, 50, 50));

        /**fin desarrollo de la alineacion**/

        add(new JButton("Amarillo"));
        add(new JButton("Rojo"));
        add(new JButton("Azul"));
    }
}
