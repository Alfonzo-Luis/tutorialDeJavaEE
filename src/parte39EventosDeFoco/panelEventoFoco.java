package parte39EventosDeFoco;

import javax.swing.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

class panelEventoFoco extends JFrame{

    private JButton button1Button;
    private JButton button2Button;
    private JButton button3Button;
    private JButton button4Button;
    private JPanel panelEventosFoco;
    private JTextField textField1;
    private JTextField textField2;

    public panelEventoFoco(){
        setTitle("Eventos de Foco");
        setBounds(30, 30, 600, 500);
        add(panelEventosFoco);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        EventosDeFoco eventoFoco = new EventosDeFoco();

        textField1.addFocusListener(eventoFoco);
        textField2.addFocusListener(eventoFoco);

    }

    class EventosDeFoco implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            if(e.getSource() == textField1){
                System.out.println("El cuadro 1 Ha GANADO el foco");
            }else{
                System.out.println("Alguien m�s Ha GANADO el foco");
            }

        }

        @Override
        public void focusLost(FocusEvent e) {
            System.out.println("Ha PERDIDO el foco");
        }
    }


}
