package parte01introduccion;

public class ej03declaracionDeConstante {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * El modificador double es lo que define
		 * que es una constante
		 */
		final double pi = 3.1415926535; 
		
		/*
		 * Si se intenta modificar el valor
		 * de la variable mi IDE me dar� un error
		 */
		
		System.out.println(pi);
		System.out.println(Math.PI); //sacar de la API de Java

	}

}
