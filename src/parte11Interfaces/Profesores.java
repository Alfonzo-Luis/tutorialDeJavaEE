package parte11Interfaces;

/*
 * Profesores no s�lo hereda de Personas, sino que 
 * implementa la interface ParaPRofesores, que es 
 * como si tuviera doble herencia y est� obligado
 * a implementar todos los m�todos de la interface
 * - Se pueden implementar la cantidad de interfaces que necesitemos
 */
class Profesores extends Personas implements ParaProfesores{

	private String carrera;

	public Profesores(int dni, String nombre, String carrera) {
		super(dni, nombre);
		this.carrera=carrera;
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Profesor [CARRERA = " + carrera + ", NOMBRE = " + super.getNombre() +  ", DNI = " + super.getDni() + "]";
	}

	@Override
	public void modificarPromedio(Estudiante estudiante, double promedio) {
		// TODO Auto-generated method stub
		estudiante.setPromedio(promedio);
	}


}
