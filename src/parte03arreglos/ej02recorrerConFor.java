package parte03arreglos;

public class ej02recorrerConFor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int valores [] = {2,59,74,98,9};
		
		/*
		 *Esta estructura sirve si conocemos 
		 *el tama�o de nuestro array.
		 */
		
		System.out.println("Recorrido con sabiendo la cantidad de elementos:");
		
		for (int i=0 ; i<5; i++) {
			System.out.print(valores[i]+", ");
		}
		
		System.out.println("\n---------------"); //para hacer un punto y aparte
		
		/*
		 * lenght nos devuelve la cantidad de elementos 
		 * que posee el array, y por ello nos sirve para
		 * saber el l�mite superior de nuestro array
		 */
		System.out.println("Recorrido con length");
		
		for (int i=0 ; i<valores.length; i++) {
			System.out.print(valores[i]+", ");
		}
		
		System.out.println("\n---------------"); //para hacer un punto y aparte
		
		/*
		 * Pero como todo en la vida, siempre hay alguien 
		 * que lo hace mejor.
		 * Aqu� podemos ver el bucle for-each o bucle 
		 * mejorado que sirve para recorrer los array al igual que las 
		 * otras estructuras pero m�s facil. 
		 */
		System.out.println("Recorrido con For-each");
		
		for (int valor:valores) {
			System.out.print(valor+", ");
		}

	}

}
