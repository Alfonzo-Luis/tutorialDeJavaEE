package parte25Buffers;

import java.io.BufferedReader;
import java.io.FileReader;

public class parte01BufersPorDefecto {
    public static void main(String[]args){

        LeerFichero lectura = new LeerFichero();
        lectura.leeDatos();

    }
}

class LeerFichero{

    public void leeDatos() {

        try {
            FileReader entrada = new FileReader("C:/Users/facundo/eclipse-workspace/tutorialDeJavaEE/src/parte27StreamsDeDatos/archivo2.txt");

            BufferedReader miBuffer = new BufferedReader(entrada);
            String linea = "";

            while(linea != null){

                linea = miBuffer.readLine();
                System.out.println(linea);

            }

            entrada.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
