package parte48SwingJTextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 400, 400);
        add(new PanelPrincipal());
        setTitle("JTextField");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    private JTextField texto;
    private JLabel textoLabel;

    public PanelPrincipal(){

        texto = new JTextField(20);
        add(texto);

        JButton boton = new JButton("Ejecutar");

        ObtenerTexto obtener = new ObtenerTexto();

        textoLabel = new JLabel();

        boton.addActionListener(obtener);

        add(boton);

        add(textoLabel);

    }

    private class ObtenerTexto implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            textoLabel.setText(texto.getText());
        }
    }

}
