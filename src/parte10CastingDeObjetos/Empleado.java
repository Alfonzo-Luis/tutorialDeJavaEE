package parte10CastingDeObjetos;

class Empleado {
	
	private int dni; 
	private String nombre;
	private static String facultad ="UTN_FRR";
	
	public Empleado(int dni, String nombre) {
		this.dni=dni;
		this.nombre=nombre;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Empleado [dni=" + dni + ", nombre=" + nombre + "]";
	}
	
	/*
	 * M�todo est�tico: para probar como hacerlo
	 * NO necesita que la clase sea instanciada para ser utilizado. 
	 */
	
	public static String getFacultad() {
		return facultad;
		
	}

}
