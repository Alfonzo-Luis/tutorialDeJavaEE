package parte27Serializacion;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.TreeSet;

class parte2Deserializacion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			
			ObjectInputStream flujoDeEntrada = new ObjectInputStream(new FileInputStream(
					"C:/Users/facundo/eclipse-workspace/tutorialDeJavaEE/src/parte24Serializacion/empleados.dat"));
			/*
			 * Casteamos la entrada ya que como recibe un ibjeto del tipo Object
			 * debemos convertirlo en uno del tipo Empleado
			 */
			
			TreeSet<Empleado> objetoDeEntrada = (TreeSet<Empleado>) flujoDeEntrada.readObject();
			
			System.out.println("LEER: "+objetoDeEntrada.toString());
			
			/*
			 * Una vez terminado de usar debemnos cerrar
			 */
			
			flujoDeEntrada.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
