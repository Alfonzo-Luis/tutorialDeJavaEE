package parte27Serializacion;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.TreeSet;

class parte01Serializacion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TreeSet<Empleado> empleados = new TreeSet<>();

		empleados.add(new Empleado(1245, "Carlos"));
		empleados.add(new Empleado(3574, "Anal�a"));
		empleados.add(new Empleado(3698, "Pedro"));
		empleados.add(new Empleado(1325, "Rita"));

		System.out.println("GUARDAR: "+empleados.toString());

		/*
		 * Guardando en el disco un objeto con ObjectOutputStream guardamos el objeto
		 * que queremos serializar indicandole el lugar.
		 */

		try {
			ObjectOutputStream flujoDeSalida = new ObjectOutputStream(new FileOutputStream(
					"C:/Users/facundo/eclipse-workspace/tutorialDeJavaEE/src/parte27Serializacion/empleados.dat"));

			/*
			 * Indicamos cual es el objeto a serializar utiliznado el flujoDeSalida siempre
			 * y cuando el objeto en cuesti�n implemente la interface serializable
			 */

			flujoDeSalida.writeObject(empleados);
			
			/*
			 * Terminamos de escribir y si no lo volveremos a usar
			 * lo cerramos. 
			 */
			flujoDeSalida.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
