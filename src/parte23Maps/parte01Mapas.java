package parte23Maps;

import java.util.HashMap;
import java.util.Map;

class parte01Mapas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * String = clave
		 * Estudiante = valor
		 */
		HashMap<String, Estudiante> estudiantes = new HashMap<String, Estudiante>();
		
		/*
		 * AGREGAR ELEMENTOS:
		 * La clave no puede repetirse y peude ser de cualquier tipo
		 */
		estudiantes.put("2",new Estudiante("Agustin", 3528));
		estudiantes.put("9",new Estudiante("Pedrito", 3528));
		estudiantes.put("10",new Estudiante("Bella", 3528));
		estudiantes.put("5",new Estudiante("Gustavo", 3528));

		/*
		 * Nos devolver� los elementos ordenados seg�n su clave.
		 */
		System.out.println(estudiantes);
		
		/*
		 * Para ELIMINAR un elemento debemos indicar su clave
		 */
		
		estudiantes.remove("9");
		
		System.out.println(estudiantes);
		
		/*
		 * SOBRE ESCRIBIR ELEMENTOS
		 */
		
		estudiantes.put("5",new Estudiante("Perie", 3528));
		
		System.out.println(estudiantes);
		
		/*
		 * RECORRER LA COLECCI�N
		 */
		
		for (Map.Entry<String, Estudiante>estudiante:estudiantes.entrySet()) {
			String clave = estudiante.getKey();
			Estudiante valor = estudiante.getValue();
			System.out.println("Clave: "+clave+" Valor: "+valor);
		}
		
	}

}

/*
 * Lo primero que hacemos es crear el objeto 
 * que colocaremos en nuestras colecciones
 */

class Estudiante{
	
	private String nombre;
	private int legajo;
	
	public Estudiante(String nombre, int legajo) {
		super();
		this.nombre = nombre;
		this.legajo = legajo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	@Override
	public String toString() {
		return "Estudiante [nombre=" + nombre + ", legajo=" + legajo + "]";
	}
	
	
}
