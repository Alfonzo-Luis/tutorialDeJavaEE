package parte32InterfacesGraficas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class parte01EventosConVariosBotones {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        VentanasPrincipales ventana = new VentanasPrincipales();
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

class VentanasPrincipales extends JFrame {

    public VentanasPrincipales() {

        setBounds(5, 50, 500, 400);
        PanelesConBotones panel = new PanelesConBotones();
        add(panel);
        setVisible(true);
    }
}

//implementar ActionListener para escuchar la accion
class PanelesConBotones extends JPanel implements ActionListener {

    JButton botonAzul = new JButton("AZUL");
    JButton botonRojo = new JButton("ROJO");
    JButton botonVerde = new JButton("VERDE");

    public PanelesConBotones() {
        add(botonAzul);
        add(botonRojo);
        add(botonVerde);
        botonAzul.addActionListener(this);
        botonRojo.addActionListener(this);
        botonVerde.addActionListener(this);
    }


    //metodo de la interface ActionListener
    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        Object btnApretado = e.getSource(); //nos trae el objeto que fue apretado
        if (btnApretado == botonAzul) setBackground(Color.BLUE);
        else if (btnApretado == botonRojo) setBackground(Color.RED);
        else setBackground(Color.GREEN);
    }

    /* si sacas los comentarios de este texto podr�s ver m�s acciones posibles*/

    /*
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        Object btnApretado = e.getSource(); //nos trae el objeto que fue apretado
        if (btnApretado == botonAzul){
            setBackground(Color.BLUE);
            botonAzul.setText("APRETADO");
        } else {
            if (btnApretado == botonRojo){
                setBackground(Color.RED);
                botonRojo.setText("APRETADO");
            } else {
                setBackground(Color.GREEN);
                botonVerde.setText("APRETADO");
            }
        }
    }
    */

}
