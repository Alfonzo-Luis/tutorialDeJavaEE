package parte07UsoEstudiantesConHerencia;

class Persona {
	
	/*
	 * Clase agregada para ver la HERENCIA de objetos
	 */
	
	private String nombre;

	public String getNombrePersona() {
		return nombre;
	}

	public void setNombrePersona(String nombre) {
		this.nombre = nombre;
	}
	
	

}
