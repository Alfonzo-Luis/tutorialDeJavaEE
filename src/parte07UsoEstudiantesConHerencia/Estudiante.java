package parte07UsoEstudiantesConHerencia;

class Estudiante extends Persona{
	
	private Carrera carreraEstudiante;
	private double promedio;
	
	private int nroLegajo;
	private static int LegajoAlmacenado=1;
	
	public Estudiante(Carrera carreraEstudiante, String nombre, double promedio) {

		this.carreraEstudiante = carreraEstudiante;
		super.setNombrePersona(nombre);
		this.promedio = promedio;
		this.nroLegajo = LegajoAlmacenado;
		Estudiante.LegajoAlmacenado++;
	}
	
	public void setPromedio(double promedio) {
		this.promedio=promedio;
	}

	@Override
	public String toString() {
		return "Estudiante [carreraEstudiante=" + carreraEstudiante + ", nombre=" + super.getNombrePersona() + ", promedio=" + promedio
				+ ", nroLegajo=" + nroLegajo + "]";
	}
	
	private String getNombreCarrera() {
		return this.carreraEstudiante.getNombreCarrera();
	}
	
	public String getNombreEstudiante() {
		return super.getNombrePersona();
	}

}
