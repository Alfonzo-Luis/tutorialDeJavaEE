package parte07UsoEstudiantesConHerencia;

public class UsoFacultar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Carrera tsp = new Carrera("TSP", 20);
		
		tsp.nuevoEstudiante("Carlos", 8);
		tsp.nuevoEstudiante("Pedro", 7);
		tsp.nuevoEstudiante("Juan", 6);
		tsp.nuevoEstudiante("Raul", 9);
		
		tsp.getEstudiantes();
		
		tsp.borrarEstudiante("Pedro");
		
		tsp.getEstudiantes();
		
		tsp.getDatosEstudiante("Juan");
		
		tsp.setPromedio("Raul", 10);
		
		tsp.getDatosEstudiante("Raul");
	}

}
